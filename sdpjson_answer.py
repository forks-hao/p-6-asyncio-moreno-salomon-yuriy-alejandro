import asyncio
import sdp_transform
import json
class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))

        dict = json.loads(message)
        dict['sdp'] = sdp_transform.parse(dict['sdp'])
        dict['sdp']['media'][0]['port'] = 34543
        dict['sdp']['media'][1]['port'] = 34543
        dict['type'] = 'answer'
        dict['sdp'] = sdp_transform.write(dict['sdp'])
        message = json.dumps(dict)
        print('Send %r to %s' % (message, addr))

        self.transport.sendto(message.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())