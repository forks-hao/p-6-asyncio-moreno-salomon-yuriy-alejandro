import asyncio
import sdp_transform

# parse str to dict


class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())

        print("Close the socket")
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()

    sdp_document = {'version': 0,
  'origin': {'username': 'user',
    'sessionId': 434344,
    'sessionVersion': 0,
    'netType': 'IN',
    'ipVer': 4,
    'address': '127.0.0.1'},
  'name': 'Session',
  'timing': {'start': 0,
    'stop': 0},
  'connection': {'version': 4,
    'ip': '127.0.0.1'},
  'media': [{'rtp': [{'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                     {'payload': 96, 'codec': 'opus', 'rate': 48000}],
             'type': 'audio',
             'port': 54400,
             'protocol': 'RTP/SAVPF',
             'payloads': '0 96',
             'ptime': 20,
             'direction': 'sendrecv'},
            {'rtp': [{'codec': 'H264', 'payload': 97, 'rate': 90000},
                    {'codec': 'VP8', 'payload': 98, 'rate': 90000}],
             'type': 'video',
             'port': 55400,
             'protocol': 'RTP/SAVPF',
             'payloads': '97 98',
             'direction': 'sendrecv'}]}

    message = sdp_transform.write(sdp_document)

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(message, on_con_lost),
        remote_addr=('127.0.0.1', 9999))

    try:
        await on_con_lost
    finally:
        transport.close()


asyncio.run(main())